﻿using UnityEngine;
using System.Collections;

public class MoveButtonBehavior : MonoBehaviour
{
    public GameObject brain;
    public GameObject yawbutton;
    public GameObject pitchbutton;
    public GameObject rollbutton;

    [Tooltip("Audio clip to play when interacting with this hologram.")]
    public AudioClip TargetFeedbackSound;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        EnableAudioHapticFeedback();
    }

    private void EnableAudioHapticFeedback()
    {
        // If this hologram has an audio clip, add an AudioSource with this clip.
        if (TargetFeedbackSound != null)
        {
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = TargetFeedbackSound;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 1;
            audioSource.dopplerLevel = 0;
        }
    }

    // Update is called once per frame
    void OnSelect()
    {
        if (audioSource != null && !audioSource.isPlaying)
        {
            audioSource.Play();
        }

        GestureManager.Instance.ResetGestureRecognizers();

        gameObject.GetComponent<TextMesh>().color = Color.red;
        pitchbutton.GetComponent<TextMesh>().color = Color.white;
        rollbutton.GetComponent<TextMesh>().color = Color.white;
        yawbutton.GetComponent<TextMesh>().color = Color.white;
    }
}

