﻿using UnityEngine;
using System.Collections;

public class WindowButtonBehavior : MonoBehaviour {
    bool placeFinish = false;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void OnSelect () {
        if(!placeFinish)
        {
            gameObject.SendMessageUpwards("OnPlace");
            placeFinish = true;
        }
	}
}
