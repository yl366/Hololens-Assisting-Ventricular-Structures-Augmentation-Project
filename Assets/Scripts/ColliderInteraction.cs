﻿using UnityEngine;
using System.Collections;

public class ColliderInteraction : MonoBehaviour {
    Color defaultcolor;
    private AudioSource audiosource;
    public AudioClip clip;
    void Start()
    {
        defaultcolor = gameObject.GetComponent<Renderer>().material.color;
        audiosource = gameObject.AddComponent<AudioSource>();
        audiosource.clip = clip;
    }
	void OnTriggerEnter(Collider other)
    {
        //Debug.Log("Enter OnTrigger");
        if (other.gameObject.tag == "Catheter")
        {
            gameObject.GetComponent<Renderer>().material.color = Color.red;
        }

        audiosource.Play();
        
    }

    void OnTriggerExit(Collider other)
    {
        //Debug.Log("Exit OnTrigger");
        if (other.gameObject.tag == "Catheter")
        {
            gameObject.GetComponent<Renderer>().material.color = defaultcolor;
        }
    }
}
