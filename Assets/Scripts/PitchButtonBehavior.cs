﻿using UnityEngine;
using System.Collections;

public class PitchButtonBehavior : MonoBehaviour
{
    public GameObject brain;
    public GameObject movebutton;
    public GameObject yawbutton;
    public GameObject rollbutton;

    [Tooltip("Audio clip to play when interacting with this hologram.")]
    public AudioClip TargetFeedbackSound;
    private AudioSource audioSource;

    // Use this for initialization
    void Start()
    {
        EnableAudioHapticFeedback();
    }

    private void EnableAudioHapticFeedback()
    {
        // If this hologram has an audio clip, add an AudioSource with this clip.
        if (TargetFeedbackSound != null)
        {
            audioSource = GetComponent<AudioSource>();
            if (audioSource == null)
            {
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            audioSource.clip = TargetFeedbackSound;
            audioSource.playOnAwake = false;
            audioSource.spatialBlend = 1;
            audioSource.dopplerLevel = 0;
        }
    }

    // Update is called once per frame
    void OnSelect()
    {
        if (audioSource != null && !audioSource.isPlaying)
        {
            audioSource.Play();
        }

        GestureManager.Instance.Transition(GestureManager.Instance.NavigationRecognizer);
        GestureAction.Instance.EnablePitchMode();

        gameObject.GetComponent<TextMesh>().color = Color.red;
        yawbutton.GetComponent<TextMesh>().color = Color.white;
        rollbutton.GetComponent<TextMesh>().color = Color.white;
        movebutton.GetComponent<TextMesh>().color = Color.white;
    }
}

