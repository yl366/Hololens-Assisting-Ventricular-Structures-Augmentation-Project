﻿using UnityEngine;
using UnityEngine.VR.WSA.Persistence;
using UnityEngine.VR.WSA;

public class WorldAnchor : MonoBehaviour {
    private GameObject brain;
    WorldAnchorStore anchorStore = null;
    const string AnchorID = "brain";

    void Awake()
    {
        brain=GameObject.FindWithTag("Brain");
    }
    // Use this for initialization
    
    void Anchor ()
    {
        WorldAnchorStore.GetAsync(AnchorStoreReady);
        print("enter anchor");
    }

    void AnchorStoreReady(WorldAnchorStore store)
    {
        CreateAnchor();
        anchorStore = store;
        anchorStore.Load(AnchorID, brain);
        print("enter store ready");
    }

    void CreateAnchor()
    {
        brain.AddComponent<UnityEngine.VR.WSA.WorldAnchor>();
        print("enter create anchor");       
    }
    
    void DeleteAnchor()
    {
        DestroyImmediate(brain.GetComponent<UnityEngine.VR.WSA.WorldAnchor>());
        anchorStore.Delete(AnchorID);
    }
}
