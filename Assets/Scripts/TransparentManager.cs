﻿using UnityEngine;
using System.Collections;

public class TransparentManager : MonoBehaviour {
    public float level = 0;
	// Use this for initialization
	void Start () {
        Color color = gameObject.GetComponent<Renderer>().material.color;
        color.a = level;
        gameObject.GetComponent<Renderer>().material.color = color;
    }
}
