﻿using UnityEngine;
using System.Collections;
using HoloToolkit;
using UnityEngine.VR.WSA.Input;
/// <summary>
/// GestureAction performs custom actions based on 
/// which gesture is being performed.
/// </summary>
public class GestureAction : Singleton<GestureAction>
{
    [Tooltip("Rotation max speed controls amount of rotation.")]

    private Vector3 manipulationPreviousPosition;

    private float rotationFactor;

    public float RotationSensitivity = 10.0f;

    public float MoveScale=1.0f;

    bool isPitch = false;
    bool isYaw = false;
    bool isRoll = false;

    void Update()
    {
        
        PerformRotation();
    }

    private void PerformRotation()
    {
        if (GestureManager.Instance.IsNavigating &&
            HandsManager.Instance.FocusedGameObject == gameObject)
        {
          
            rotationFactor = GestureManager.Instance.NavigationPosition.x * RotationSensitivity;
            
            if (isPitch == true)
            {
                transform.Rotate(new Vector3(0, 0, -1 * rotationFactor), Space.Self);
            }

            else if (isRoll==true)
            {
                transform.Rotate(new Vector3(rotationFactor, 0, 0), Space.Self);               
            }

            else if(isYaw==true)
            {
                transform.Rotate(new Vector3(0, -1 * rotationFactor, 0),Space.Self);
            }           
        }
    }

    public void EnablePitchMode()
    {
        isPitch = true;
        isRoll = false;
        isYaw = false;
    }

    public void EnableRollMode()
    {
        isRoll = true;
        isPitch = false;
        isYaw = false;
    }

    public void EnableYawMode()
    {
        isYaw = true;
        isRoll = false;
        isPitch = false;
    }


    public  void PerformManipulationStart(Vector3 position)
    {
        manipulationPreviousPosition = position;
        
    }

    void PerformManipulationUpdate(Vector3 position)
    {
        if (GestureManager.Instance.IsManipulating)
        {
            
            Vector3 moveVector = position - manipulationPreviousPosition;
            
            manipulationPreviousPosition = position;
            
            transform.position += moveVector * MoveScale;
        }
    }
}